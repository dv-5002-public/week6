/// <reference types="Cypress" />
describe('My First Test', function () {
    it('Assignment 1', function () {
        cy.visit('https://camt.cmu.ac.th/th')
        cy.contains('เกี่ยวกับเรา').click()
        cy.contains('โครงสร้างวิทยาลัยฯ')
            .click()
            cy.get('#Image135')
            .click()
            cy.contains('ผู้ช่วยศาสตราจารย์ ดร.ชาติชาย ดวงสอาด')
            .click()
    })
})