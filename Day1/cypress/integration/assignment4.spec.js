/// <reference types="Cypress" />
describe('Fill the form test', () => {
    it('Assignment 4 Case 1: THB to USD', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('500')
        cy.get('#selectTarget').click()
        cy.get('#targetUSD').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 2: THB to EUR', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('100')
        cy.get('#selectTarget').click()
        cy.get('#targetEUR').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 3: USD to EUR', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('200')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetEUR').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 4: USD to THB', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('300')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 5: EUR to THB', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('400')
        cy.get('#selectSource').click()
        cy.get('#sourceEUR').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 6: EUR to USD', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('600')
        cy.get('#selectSource').click()
        cy.get('#sourceEUR').click()
        cy.get('#selectTarget').click()
        cy.get('#targetUSD').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 7: THB to THB', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('700')
        cy.get('#selectTarget').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 8: USD to USD', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('800')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetUSD').click()
        cy.get('#calBtn').click()
    })

    it('Assignment 4 Case 9: EUR to EUR', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('900')
        cy.get('#selectSource').click()
        cy.get('#sourceEUR').click()
        cy.get('#selectTarget').click()
        cy.get('#targetEUR').click()
        cy.get('#calBtn').click()
    })

    Cypress.on('uncaught:exception',(err,runnable)=>{
        return false
    })
})