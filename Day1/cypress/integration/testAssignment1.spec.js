/// <reference types="Cypress" />
describe('Fill the form test', () => {
    it('Test Case 1: USD to THB input number', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('30')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', '900.00 THB')
            .and('have.class', 'alert alert-success')
    })

    it('Test Case 2: USD to THB input -1', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('-1')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', 'Please input positive number')
            .and('have.class', 'alert alert-success')
    })

    it('Test Case 3: USD to THB input text', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('a')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', 'Please input number')
            .and('have.class', 'alert alert-success')
    })

    it('Test Case 4: USD to THB input decimal number', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('2.05')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', '61.50 THB')
            .and('have.class', 'alert alert-success')
    })

    it('Test Case 4: USD to THB input 0', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount').type('0')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', '0 THB')
            .and('have.class', 'alert alert-success')
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })
})