async function loadAnimeAsync() {
    let response = await fetch('https://api.jikan.moe/v3/search/anime?q=fate%20stay%20night%20unlimited&limit=1')
    let data = await response.json()
    return data

}

function createResultAnime(data) {
    let resultElement = document.getElementById('resultAnime')
    resultElement.innerHTML = ''
    console.log(data)

    data.then((json) => {
        for (let i = 0; i < json["results"].length; i++) {
            var currentData = json["results"][i]

            let card = document.createElement('div')
            card.setAttribute('class', 'card')
            card.style.marginBottom = '5%'
            card.style.background = 'url(http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/dark_wall.png)'
            card.style.boxShadow = '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)'
            resultElement.appendChild(card)

            let row = document.createElement('div')
            row.setAttribute('class', 'row')
            card.appendChild(row)

            let imgNode = document.createElement('div')
            row.appendChild(imgNode)
            let img = document.createElement('img')
            img.setAttribute('src', currentData['image_url'])
            img.style.marginLeft = '6.5%'
            imgNode.appendChild(img)

            let infoNode = document.createElement('div')
            infoNode.setAttribute('class', 'col')
            row.appendChild(infoNode)
            let infoCard = document.createElement('div')
            infoCard.style.margin = '4%'

            infoNode.appendChild(infoCard)

            let title = document.createElement('h4')
            title.innerText = currentData['title']
            title.setAttribute('class', 'card-title')
            infoCard.appendChild(title)

            let text = document.createElement('p')
            text.innerText = currentData['synopsis']
            text.setAttribute('class', 'card-text')
            infoCard.appendChild(text)

            let episodes = document.createElement('p')
            let episodesText = document.createElement('p')
            episodesText.innerText = 'Episodes: '
            episodes.innerHTML = currentData['episodes']
            episodes.style.marginTop = '-8.5%'
            episodes.style.marginLeft = '15%'
            infoCard.appendChild(episodesText)
            infoCard.appendChild(episodes)

            let type = document.createElement('p')
            let typeText = document.createElement('p')
            typeText.innerText = 'Type: '
            type.style.marginTop = '-8.5%'
            type.style.marginLeft = '10%'
            type.innerHTML = currentData['type']
            infoCard.appendChild(typeText)
            infoCard.appendChild(type)

            let readMoreDiv = document.createElement("div")
            let readMore = document.createElement("a");
            readMoreLink = document.createElement("a");
            readMore.setAttribute("href", currentData['url']);
            readMore.setAttribute("class", "btn btn-outline-light");
            readMoreDiv.style.textAlign = "right";
            readMoreDiv.style.marginRight = '5%'
            readMoreDiv.style.marginBottom = '5%'
            readMore.innerHTML = "Read more";
            readMoreDiv.appendChild(readMore);
            infoCard.appendChild(readMoreDiv);
        }
    })
}

async function loadAllAnimes() {
    let name = document.getElementById('queryId').value
    if (name != '' && name != null) {
        let response = await fetch('https://api.jikan.moe/v3/search/anime?q=' + name)
        return response.json()
    }
}